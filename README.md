# docstrip

Docstrip is a tool for creating packages and their documentation in an elegant, literate programming scheme.

The syntax is sort of similar to the TeX docstrip utility, by which **all comments** ~~are~~ ***can be* removed.** Markdown and Javascript files are generated with shared or respectively designated comments. Every non-commented line or block is implicitly javascript.

### Basic Usage and File Structure

Basic usage involves a source file and the docstrip script file. This package, for example, places the docstrip (`docstrip.js`) and source (`index.js`) files within the `/docstrip` subdirectory, and the destination file is aimed at the top level (which happens to also be named `docstrip`).

```
Project
 |__ docstrip
 |    |__ docstrip.js
 |    |__ source.js
 |__ output.js
 |__ output.md
```

### Concurrent File Structures

Removable comments not only facilitates the organization of file type-specific content, but it also allows for the possibility of protected (or *disappearing*) comments. This means a project could be built in a private local directory, then stripped to a more navigable (and publishable) version of the project.

```
Project
 |__ Private
 |    |__ docstrip.js
 |    |__ source.js
 |__ Public
      |__ output.js
      |__ output.md
```

## Source File

### Single Lines

A line beginning with `//` such as ...

```javascript
// my comment here
```

will be rendered in place in both markdown and javascript destinations.

A line beginning with `//.` will render to javascript only.

```javascript
//. my javascript comment here
```

A line beginning with `//m` such as ...

```javascript
//m my markdown here
```

will be rendered to markdown only, and excluded in any javascript destination.

White-space-only lines are always removed. This means your source can make use of white space for legibility, while the output file(s) -- the code ultimately used -- is reduced (though not *minimized*).

### Blocks

Lines between the delimeters `///.` and `//./` will render to javascript only.

```javascript
///.

let self = 1;

function me(x) {
  return x * self;
}

//./
```

Lines between the delimeters `///m` and `//m/` will render to markdown only.

```javascript
///m

## foo

Here is some markdown stuff.

//m/
```

## docstrip script

Put the necessary lines from the following all in one file, then `nodee <that file>`.

```javascript
var ds = require('docstrip');
```



It might eventually be useful/necessary to have some sort of package options protocol, and at that point surely the distinction of `.javascript`/`.markdown`/`etc.` functionality will be essential. For now it may be a little tedious, but you can of course wrap these with your own conveninece functions, which is sort of the aim anyway.


### render markdown

```javascript
ds.markdown("../original/index.js", "../new/README.md");
```


### render javascript

```javascript
ds.javascript("../original/index.js", "../new/index.js");
```


