// # docstrip

///m

Docstrip is a tool for creating packages and their documentation in an elegant, literate programming scheme.
//
The syntax is sort of similar to the TeX docstrip utility, by which **all comments** ~~are~~ ***can be* removed.** Markdown and Javascript files are generated with shared or respectively designated comments. Every non-commented line or block is implicitly javascript.
//
### Basic Usage and File Structure
//
Basic usage involves a source file and the docstrip script file. This package, for example, places the docstrip (`docstrip.js`) and source (`index.js`) files within the `/docstrip` subdirectory, and the destination file is aimed at the top level (which happens to also be named `docstrip`).
//
```
Project
 |__ docstrip
 |    |__ docstrip.js
 |    |__ source.js
 |__ output.js
 |__ output.md
```
//
### Concurrent File Structures
//
Removable comments not only facilitates the organization of file type-specific content, but it also allows for the possibility of protected (or *disappearing*) comments. This means a project could be built in a private local directory, then stripped to a more navigable (and publishable) version of the project.
//
```
Project
 |__ Private
 |    |__ docstrip.js
 |    |__ source.js
 |__ Public
      |__ output.js
      |__ output.md
```
//
## Source File
//
### Single Lines
//
A line beginning with `//` such as ...
//
```javascript
// // my comment here
```
//
will be rendered in place in both markdown and javascript destinations.
//
A line beginning with `//.` will render to javascript only.
//
```javascript
// //. my javascript comment here
```
//
A line beginning with `//m` such as ...
//
```javascript
// //m my markdown here
```
//
will be rendered to markdown only, and excluded in any javascript destination.
//
White-space-only lines are always removed. This means your source can make use of white space for legibility, while the output file(s) -- the code ultimately used -- is reduced (though not *minimized*).
//
### Blocks
//
Lines between the delimeters `///.` and `//./` will render to javascript only.
//
```javascript
// ///.
//
let self = 1;
//
function me(x) {
  return x * self;
}
//
// //./
```
//
Lines between the delimeters `///m` and `//m/` will render to markdown only.
//
```javascript
// ///m
//
## foo
//
Here is some markdown stuff.
//
// //m/
```
//
## docstrip script
//
Put the necessary lines from the following all in one file, then `nodee <that file>`.
//
```javascript
var ds = require('docstrip');
```



//m/








///.

const doc = module.exports;

// require node's filesystem
var fs = require('fs');

// some global variables
var entry, prelist, list, rawlen, comments, newdoc, len;
var jsblock = false;
var mdblock = false;

// array/stringify the input file
function processfile(file) {
  entry    = fs.readFileSync(file, 'utf8');
  prelist  = entry.toString();
  list     = entry.toString().split(/\r\n|\r|\n/);
  rawlen   = list.length;
  comments = [];
  newdoc   = "";
}

// test, beginsWith ( 4 )
let beginsWith = function(line, test) {
  if (line.substring(0,4) === test) {
    return true; }
  else { return false; }
}

// test if line starts/ends a block
function blocktest(line) {
  if (beginsWith(line, '///.')) { jsblock = true; }
  if (beginsWith(line, '//./')) { jsblock = false; }
  if (beginsWith(line, '///m')) { mdblock = true; }
  if (beginsWith(line, '//m/')) { mdblock = false; }
}

function delimitsMarkdown(line) {
  if (beginsWith(line, '///m') || beginsWith(line, '//m/')) {
    return true; }
  else { return false; }
}

function delimitsJS(line) {
  if (beginsWith(line, '///.') || beginsWith(line, '//./')) {
    return true; }
  else { return false; }
}

function commentsMarkdown(line) {
  if (beginsWith(line, '//m ')) {
    return true; }
  else { return false; }
}

function commentsJS(line) {
  if (beginsWith(line, '//. ')) {
    return true; }
  else { return false; }
}


//./










///m
It might eventually be useful/necessary to have some sort of package options protocol, and at that point surely the distinction of `.javascript`/`.markdown`/`etc.` functionality will be essential. For now it may be a little tedious, but you can of course wrap these with your own conveninece functions, which is sort of the aim anyway.
//m/







//
// ### render markdown

///m
```javascript
ds.markdown("../original/index.js", "../new/README.md");
```
//m/

//. the following block should be hidden from markdown
///.
doc.markdown = function(file, newfile) {

  processfile(file);

  for (i = rawlen-1; i >= 0; i--) {
    if (list[i] == "" || commentsJS(list[i])) {
      list.splice(i, 1);
    }
  }

  len = list.length;

  for (i=0; i<len; i++) {

    let line = list[i];

    blocktest(line);

    if (mdblock) {

      comments[comments.length] = true;

      if (delimitsMarkdown(line)) {
        line = line.slice(4);
      } else {
        if (line.substring(0,3) === "// ") {
          line = line.slice(3);
        } else {
          if (line.substring(0,2) === "//" ) {
            line = line.slice(2);
          }
        }
      }

      newdoc += line + "\n";

    } else {

      if (!jsblock) {

        if (line.substring(0,2) === "//") {
          comments[comments.length] = true;
          //for markdown, remove "//"
          if (commentsMarkdown(line) || delimitsMarkdown(line)) {
            line = line.slice(4);
          } else {
            if (delimitsJS(list[i])) {
              line = line.slice(4);
            } else {
              line = line.slice(3);
            }
          }
          if (comments.length > 1) {
            if (!comments[comments.length - 2]) {
              // line before isn't also a comment
              newdoc += "'''\n" + line + "\n";
            } else {
              // life before is also a comment
              newdoc += line + "\n";
            }
          } else {
            newdoc += line + "\n";
          }
        }
        // else it is code
        else {
          comments[comments.length] = false;
          if (comments.length > 1) {
            if (comments[comments.length - 2]) {
              // if the line before is a comment
              newdoc += "'''\n" + line + "\n";
            } else {
              // line before isn't a comment
              newdoc += line + "\n";
            }
          } else {
            newdoc += line + "\n";
          }
        }
      }
    }
  }

  fs.writeFileSync(newfile, newdoc);

}
//./









// ### render javascript

///m
```javascript
ds.javascript("../original/index.js", "../new/index.js");
```
//m/

///.
doc.javascript = function(file, newfile) {

  processfile(file);

  for (i = rawlen-1; i >= 0; i--) {
    if (list[i] == "" || commentsMarkdown(list[i]) || delimitsJS(list[i])) {
      list.splice(i, 1);
    }
  }

  len = list.length;

  for (i=0; i<len; i++) {

    let line = list[i];

    blocktest(line);

    // if not a mdblock
    if (!mdblock) {
      // if it is a comment
      if (line.substring(0,2) === "//") {
        comments[comments.length] = true;
        if (!delimitsMarkdown(line)) {
          if (commentsJS(line)) {
            newdoc += "// " + line.slice(3) + "\n";
          } else {
            newdoc += line + "\n";
          }
        }
      } else {
        // else it is code
        if (!delimitsMarkdown(line)) {
          comments[comments.length] = false;
          newdoc += line + "\n";
        }
      }
    }
  }

  fs.writeFileSync(newfile, newdoc);

}
//./
