
var ds = require("../index.js");
// Normally, usage is:
//   var docstrip = require('docstrip');

// Strip the document to markdown:
ds.markdown("./index.js", "../README.md");
// Strip the document to javascript:
ds.javascript("./index.js", "../index.js");

// 'cd' into this directory and 'node <this file>'
