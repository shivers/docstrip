// # docstrip
const doc = module.exports;
// require node's filesystem
var fs = require('fs');
// some global variables
var entry, prelist, list, rawlen, comments, newdoc, len;
var jsblock = false;
var mdblock = false;
// array/stringify the input file
function processfile(file) {
  entry    = fs.readFileSync(file, 'utf8');
  prelist  = entry.toString();
  list     = entry.toString().split(/\r\n|\r|\n/);
  rawlen   = list.length;
  comments = [];
  newdoc   = "";
}
// test, beginsWith ( 4 )
let beginsWith = function(line, test) {
  if (line.substring(0,4) === test) {
    return true; }
  else { return false; }
}
// test if line starts/ends a block
function blocktest(line) {
  if (beginsWith(line, '///.')) { jsblock = true; }
  if (beginsWith(line, '//./')) { jsblock = false; }
  if (beginsWith(line, '///m')) { mdblock = true; }
  if (beginsWith(line, '//m/')) { mdblock = false; }
}
function delimitsMarkdown(line) {
  if (beginsWith(line, '///m') || beginsWith(line, '//m/')) {
    return true; }
  else { return false; }
}
function delimitsJS(line) {
  if (beginsWith(line, '///.') || beginsWith(line, '//./')) {
    return true; }
  else { return false; }
}
function commentsMarkdown(line) {
  if (beginsWith(line, '//m ')) {
    return true; }
  else { return false; }
}
function commentsJS(line) {
  if (beginsWith(line, '//. ')) {
    return true; }
  else { return false; }
}
//
// ### render markdown
//  the following block should be hidden from markdown
doc.markdown = function(file, newfile) {
  processfile(file);
  for (i = rawlen-1; i >= 0; i--) {
    if (list[i] == "" || commentsJS(list[i])) {
      list.splice(i, 1);
    }
  }
  len = list.length;
  for (i=0; i<len; i++) {
    let line = list[i];
    blocktest(line);
    if (mdblock) {
      comments[comments.length] = true;
      if (delimitsMarkdown(line)) {
        line = line.slice(4);
      } else {
        if (line.substring(0,3) === "// ") {
          line = line.slice(3);
        } else {
          if (line.substring(0,2) === "//" ) {
            line = line.slice(2);
          }
        }
      }
      newdoc += line + "\n";
    } else {
      if (!jsblock) {
        if (line.substring(0,2) === "//") {
          comments[comments.length] = true;
          //for markdown, remove "//"
          if (commentsMarkdown(line) || delimitsMarkdown(line)) {
            line = line.slice(4);
          } else {
            if (delimitsJS(list[i])) {
              line = line.slice(4);
            } else {
              line = line.slice(3);
            }
          }
          if (comments.length > 1) {
            if (!comments[comments.length - 2]) {
              // line before isn't also a comment
              newdoc += "'''\n" + line + "\n";
            } else {
              // life before is also a comment
              newdoc += line + "\n";
            }
          } else {
            newdoc += line + "\n";
          }
        }
        // else it is code
        else {
          comments[comments.length] = false;
          if (comments.length > 1) {
            if (comments[comments.length - 2]) {
              // if the line before is a comment
              newdoc += "'''\n" + line + "\n";
            } else {
              // line before isn't a comment
              newdoc += line + "\n";
            }
          } else {
            newdoc += line + "\n";
          }
        }
      }
    }
  }
  fs.writeFileSync(newfile, newdoc);
}
// ### render javascript
doc.javascript = function(file, newfile) {
  processfile(file);
  for (i = rawlen-1; i >= 0; i--) {
    if (list[i] == "" || commentsMarkdown(list[i]) || delimitsJS(list[i])) {
      list.splice(i, 1);
    }
  }
  len = list.length;
  for (i=0; i<len; i++) {
    let line = list[i];
    blocktest(line);
    // if not a mdblock
    if (!mdblock) {
      // if it is a comment
      if (line.substring(0,2) === "//") {
        comments[comments.length] = true;
        if (!delimitsMarkdown(line)) {
          if (commentsJS(line)) {
            newdoc += "// " + line.slice(3) + "\n";
          } else {
            newdoc += line + "\n";
          }
        }
      } else {
        // else it is code
        if (!delimitsMarkdown(line)) {
          comments[comments.length] = false;
          newdoc += line + "\n";
        }
      }
    }
  }
  fs.writeFileSync(newfile, newdoc);
}
